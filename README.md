# Size Guides
### by Albano Toska
##### I created this task in form of a plugin.
##### Just download/clone the project.

#### Upload it to the site either via ftp or wordpress > add new plugin page.
#### Before activating make sure to have enabled both woocommerce and acf pro(If not the plugin will just deactivate itself until you have activated both these plugins).
#### After activating the plugin just go to products > Size Guides > Add New.
#### Upload a CSV file then the table will display