<?php
/**
 * Plugin Name: Size Guides Task
 * Description: Size Guide Task by Oceans Apart for Albano
 * Plugin URI: https://www.oceansapart.com/
 * Version: 0.1
 * Author: Albano Toska
 * Author URI: https://albanotoska.com/
 * License:     GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 */
if ( ! defined( 'ABSPATH' ) ) exit;

/* Check if both ACF Pro and Woocommerce are activated before activating this plugin */
add_action( 'admin_init', 'size_guides_check_dependancy_plugins' );
function size_guides_check_dependancy_plugins() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'woocommerce/woocommerce.php' ) || !is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
        add_action( 'admin_notices', 'size_guides_admin_error_notices' );

        deactivate_plugins( plugin_basename( __FILE__ ) ); 

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }
}


function size_guides_admin_error_notices(){
    ?><div class="error"><p>Sorry, but this plugin requires both Woocommerce and Advanced Custom Fields Pro to be Activated</p></div><?php
}

/* Register Custom Post Type Size Guides */
function size_guides_custom_post_type() {
    register_post_type( 'size-guides',
    // CPT Options
    array(
      'labels' => array(
       'name' => __( 'Size Guides' ),
       'singular_name' => __( 'Size Guide' )
      ),
      'public' => true,
      'has_archive' => false,
      'rewrite' => array('slug' => 'size-guides'),
      'show_in_rest' => true,
      'show_in_menu' => 'edit.php?post_type=product',
      'supports' => array('title')
     )
    );
}
add_action( 'init', 'size_guides_custom_post_type' );
/* Custom Post Type End */

/* Add Acf fields on plugin activation */
function add_acf_fields_on_plugin_activation() {
    if( function_exists('acf_add_local_field_group') ):

        acf_add_local_field_group(array (
            'key' => 'size_guides_custom_fields',
            'title' => 'Size Guides',
            'fields' => array (
                array (
                    'key' => 'size_guides_csv_file',
                    'label' => 'CSV File',
                    'name' => 'csv_file',
                    'type' => 'file',
                    'return_format' => 'url',
                    'prefix' => '',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                )
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'size-guides',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ));
        
    endif;
}
add_action( 'init', 'add_acf_fields_on_plugin_activation' );
/* End Acf Field adding */

/* Custom metabox to display csv file as html table */
function size_guides_csv_display_backend() {
    $screens = [ 'post', 'size-guides' ];
    foreach ( $screens as $screen ) {
        add_meta_box(
            'size_guide_render_id',
            'CSV Size Guide',
            'size_guides_render_meta_box',//metabox render function
            $screen
        );
    }
}
add_action( 'add_meta_boxes', 'size_guides_csv_display_backend' );

/* CSV parse into html table */
function size_guides_render_meta_box() {
    global $post;
    //if PHP 7 or lower we can't user str_ends_with() function to check if url has correct csv or not, since this function only works with php 8, so we declare that function
    if (!function_exists('str_ends_with')) {
        function str_ends_with($str, $end) {
          return (@substr_compare($str, $end, -strlen($end))==0);
        }
    }
    if(get_field('size_guides_csv_file', $post->ID) && str_ends_with(get_field('size_guides_csv_file', $post->ID), '.csv')) {//in order for the function render not to give any error we make sure the acf field is filled
        $csv_url = get_field('size_guides_csv_file', $post->ID);//csv url
        $csv_array = array_map('str_getcsv', file($csv_url));//we get the csv file from link from acf field and add it to an array(I could also use fgetcsv but that way we would need another while loop just to add everything on an awway, while with this function we add it directly in one line)
        $rows_count = count($csv_array);
        $counter = 0;
        echo "<table id='size_guides_from_csv' border='1'>";
        foreach ($csv_array as $rid => $row) { //loop for rows (here we can also change the function for specific case like extending or naming the top row as header and with colspan)
            echo "<tr>";
            foreach ($row as $cid => $cell) {//loop for columns
                if(empty($cell)) {//here we check if the table has empty values
                    for($i=1;$i<$rows_count;$i++) {//for loop to check through rows of same column for occurrence
                        $found=false;//declare found false if nothing is found we still want add empty <td> like for example on table head
                        if(strpos($csv_array[$rid-$i][$cid], 'X')) {//check if specific cell contains 'X' so we can find it and copy to other columns
                            echo "<td>".$csv_array[$rid-$i][$cid]."</td>";//if found copy
                            $found = true;//if found is true we dont add unneccessary <td>
                            break;
                        }
                        elseif($i==$rid) {
                            echo "<td>".$cell."</td>";//if we reached the last element and didnt find it we still add empty column(empty columns before values)
                            $found = true;//if found is true we dont add unneccessary <td>
                            break;
                        }
                    }
                    if ($found==false) {//adding empty non size columns
                        echo "<td>".$cell."</td>";
                    }
                }
                else {//if value is non empty we continue adding it as normal
                    echo "<td>".$cell."</td>";
                }
            }
            echo "</tr>\n";
        }
        echo "\n</table>";
    }
    else {
        echo "<h4>You haven't uploaded a file or you uploaded a non csv file</h4>";
    }
}

/* Optional Table Styling Enqueue just so the table can be better visible on backend */
function enqueue_size_guides_plugins_styles() {
    wp_register_style( 'admin_size_guides_table_style', plugin_dir_url( __FILE__ ) . '/assets/css/table-style.css', false, '1.0.0' );
    wp_enqueue_style( 'admin_size_guides_table_style' );
}
add_action('admin_enqueue_scripts', 'enqueue_size_guides_plugins_styles');